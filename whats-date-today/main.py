import datetime

from fastapi import FastAPI

app = FastAPI()


@app.get("/", response_model=datetime.datetime)
async def get_date():
    return datetime.datetime.utcnow()
